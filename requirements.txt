opencv-contrib-python==3.4.13.47
opencv-python==3.4.13.47
requests==2.25.1
uvicorn==0.13.3
pydantic==1.7.3
numpy==1.20.1
fastapi==0.63.0