from configparser import ConfigParser

#Get the configparser object
config_object = ConfigParser()

#Assume we need 2 sections in the config file, let's call them USERINFO and SERVERCONFIG
# config_object["VEHICLE_CONFIG"] = {
#     "vehicle_cfg": "./model_data/vehicle/yolov4_tiny_vehicle.cfg",
#     "vehicle_weights": "./model_data/vehicle/vehicle_tiny_v4.weights",
#     "vehicle_names": "./model_data/vehicle/vehicle_tiny_v4.names",
#     "vehicle_score": 0.6
# }

config_object["LICENSE_PLATE_CONFIG"] = {
    "lp_cfg": "./model_data/plate/yolov4-tiny-lp.cfg",
    "lp_weights": "./model_data/plate/lp-tiny-v4.weights",
    "lp_names": "./model_data/plate/lp.names"
}

# motorcycle_plate_cfg = "./model_data/plate/yolov4_tiny_plate.cfg"
# motorcycle_plate_weight = "./model_data/plate/plate_tiny_v4.weights"
# motorcycle_plate_name = "./model_data/plate/plate_tiny_v4.names".encode('utf8')


# # config_object["OCR_CONFIG"] = {
# #     "ocr_cfg": "./model_data/ocr/yolov4-custom-obj.cfg",
# #     "ocr_weights": "./model_data/ocr/ocr_v4.weights",
# #     "ocr_names": "./model_data/ocr/ocr_v4.names"
# # }
# config_object["OCR_CONFIG"] = {
#     "ocr_cfg": "./model_data/ocr/v3-tiny/yolov3-tiny-ocr.cfg",
#     "ocr_weights": "./model_data/ocr/v3-tiny/ocr_tiny.weights",
#     "ocr_names": "./model_data/ocr/v3-tiny/ocr.names"
# }


#Write the above sections to config.ini file
with open('config.ini', 'w') as conf:
    config_object.write(conf)


